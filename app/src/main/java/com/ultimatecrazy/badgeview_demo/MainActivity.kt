package com.ultimatecrazy.badgeview_demo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.ultimatecrazy.badgeview.BadgeView

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private var mCount: Int = 0
    private var mBtnIncrease: Button? = null
    private var mBtnIncreaseMany: Button? = null
    private var mBtnClear: Button? = null
    private var mBadgeView: BadgeView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
        initListeners()
    }

    private fun initListeners() {
        mBtnIncrease?.setOnClickListener(this)
        mBtnIncreaseMany?.setOnClickListener(this)
        mBtnClear?.setOnClickListener(this)

    }

    private fun initViews() {
        mBtnIncrease = findViewById<Button>(R.id.btnIncrease)
        mBtnIncreaseMany = findViewById<Button>(R.id.btnIncreasemany)
        mBtnClear = findViewById<Button>(R.id.btnClear)
        mBadgeView = findViewById<BadgeView>(R.id.BadgeView)
    }

    override fun onClick(v: View?) {

        when (v!!.id) {
            R.id.btnIncrease -> {
                mBadgeView!!.setNumber(++mCount, true)
            }
            R.id.btnIncreasemany -> {
                mCount += 10
                mBadgeView!!.setNumber(mCount, true)
            }
            R.id.btnClear -> {
                mCount = 0
                mBadgeView!!.setNumber(mCount, true)
            }
        }

    }
}
