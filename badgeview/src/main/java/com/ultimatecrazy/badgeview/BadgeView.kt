package com.ultimatecrazy.badgeview

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.widget.FrameLayout
import android.widget.TextView
import kotlinx.android.synthetic.main.badge_view.view.*

class BadgeView(
    context: Context,
    attrs: AttributeSet? = null
) : FrameLayout(context, attrs) {

    companion object {
        private const val DEFAULT_TEXT_COLOR = 0xFFFFFFFF
        private const val DEFAULT_TEXT_SIZE = 14
        private const val DEFAULT_ANIMATION_ENABLED = true
        private const val DEFAULT_ANIMATION_DURATION = 200
        private const val DEFAULT_MAX_TEXT_LENGTH = 3
        private const val DEFAULT_ELLIPSIZE_TEXT = "..."
    }

    var animationEnabled: Boolean = DEFAULT_ANIMATION_ENABLED
    var animationDuration: Int = DEFAULT_ANIMATION_DURATION
    var maxTextLength: Int = DEFAULT_MAX_TEXT_LENGTH
    var ellipsizeText: String = DEFAULT_ELLIPSIZE_TEXT

    var textColor: Int
        get() = txt_badge_text.currentTextColor
        set(color) = txt_badge_text.setTextColor(color)

    val textView: TextView
        get() = txt_badge_text

    var badgeBackgroundDrawable: Drawable?
        get() = img_badge_bg.drawable
        set(drawable) = img_badge_bg.setImageDrawable(drawable)

    private var isVisible: Boolean
        get() = fl_container.visibility == VISIBLE
        set(value) {
            fl_container.visibility = if (value) View.VISIBLE else INVISIBLE
        }

    private val update: Animation by lazy {
        ScaleAnimation(
            1f,
            1.2f,
            1f,
            1.2f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        ).apply {
            duration = animationDuration.toLong()
            repeatMode = Animation.REVERSE
            repeatCount = 1
        }
    }

    private val show: Animation by lazy {
        ScaleAnimation(
            0f, 1f, 0f, 1f,
            Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f
        ).apply {
            duration = animationDuration.toLong()
        }
    }

    private val hide: Animation by lazy {
        ScaleAnimation(
            1f, 0f, 1f, 0f,
            Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f
        ).apply {
            duration = animationDuration.toLong()
            setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationRepeat(animation: Animation?) {}
                override fun onAnimationEnd(animation: Animation?) {
                    isVisible = false
                }

                override fun onAnimationStart(animation: Animation?) {}
            })
        }
    }


    init {

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.badge_view, this, true)

        val attr = context.theme.obtainStyledAttributes(attrs, R.styleable.BadgeView, 0, 0)

        try {
            val textColor =
                attr.getColor(R.styleable.BadgeView_textColor, DEFAULT_TEXT_COLOR.toInt())
            txt_badge_text.setTextColor(textColor)

            val textSize = attr.getDimension(
                R.styleable.BadgeView_textSize, dpToPx(
                    DEFAULT_TEXT_SIZE
                )
            )
            txt_badge_text.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize)

            animationEnabled = attr.getBoolean(
                R.styleable.BadgeView_bvAnimationEnabled,
                DEFAULT_ANIMATION_ENABLED
            )
            animationDuration = attr.getInt(
                R.styleable.BadgeView_bvAnimationDuration,
                DEFAULT_ANIMATION_DURATION
            )

            attr.getDrawable(R.styleable.BadgeView_bvBackground)?.let {
                img_badge_bg.setImageDrawable(it)
            }

            maxTextLength =
                attr.getInt(R.styleable.BadgeView_bvMaxTextLength, DEFAULT_MAX_TEXT_LENGTH)

            attr.getString(R.styleable.BadgeView_bvEllipsizeText)?.let {
                ellipsizeText = it
            }
        } finally {
            attr.recycle()
        }
    }

    @JvmOverloads
    fun setText(text: String?, animation: Boolean = animationEnabled) {
        val badgeText = when {
            text == null -> ""
            text.length > maxTextLength -> ellipsizeText
            else -> text
        }

        if (badgeText.isEmpty()) {
            clearAnimation()
        } else if (animation) {
            if (isVisible) {
                fl_container.startAnimation(update)
            } else {
                fl_container.startAnimation(show)
            }
        }

        txt_badge_text.text = badgeText
        isVisible = true

    }


    @JvmOverloads
    fun setNumber(number: Int, animation: Boolean = animationEnabled) {
        if (number == 0) {
            clear(animation)
        } else {
            setText(number.toString(), animation)
        }
    }

    @JvmOverloads
    fun clear(animation: Boolean = animationEnabled) {
        if (!isVisible) return

        if (animation) {
            fl_container.startAnimation(hide)
        } else {
            isVisible = false
        }
    }


    private fun dpToPx(dp: Int): Float {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), resources.displayMetrics
        )
    }

}